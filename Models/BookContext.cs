using Microsoft.EntityFrameworkCore;
namespace BookManagement.Models
{
    public class BookContext : DbContext 
    {
         public BookContext(DbContextOptions<BookContext> options) : base(options)  
        {  
        }  
  
        public virtual DbSet<Book> Books { get; set; }  
       /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)  
        {  
        }  */
    }
}