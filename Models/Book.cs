using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
namespace BookManagement.Models
{
    public class Book
    {
        [Key]  
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]  
        public int Id { get; set; }
        [Required]
        public string BookName { get; set; }
        [Required]
        public string PublicationName { get; set; }  
        [Required]
        public string BookGenre { get; set; }  
        [Required]
        public string BookAuthor { get; set; }  
        [Required]
        public int Price {get; set;}
    }
}