using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BookManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookManagement.Controllers
{
    [Route("api/Book")]
    [ApiController]
    public class BookController : ControllerBase
    {       
        private BookContext _context;
        public BookController(BookContext context)
        {
            _context=context;
        }

        [HttpGet]
        [Route("get-all-books-details")]
        public ActionResult GetBooks()
        {
            try
            {
                List<Book> books = _context.Books.ToList();
                return Ok(books);  
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException);
            }            
        }
        
        [HttpGet]
        [Route("get-book-details-by-id")]  
        public ActionResult GetBook(int id)
        {   
            try
            {    
                Book books = (  from book in _context.Books
                                where book.Id == id
                                select book).FirstOrDefault();
                return Ok(books);
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException);
                //Console.LargestWindowHeight();
            }

        }

        [HttpPost]
        [Route("create-new-book")]
        public ActionResult CreateBook(Book book)
        {            
            try
            {              
                if (ModelState.IsValid)
                _context.Books.Add(book);
                _context.SaveChanges();
                 return Ok(book);           
            }

            catch (Exception e)
            {
                return BadRequest(e.InnerException);
            }
           
        }
        
        [HttpPut]
        [Route("update-book-details")]
        public ActionResult UpdateBook(Book book)
        {
            try{
            Book BookToUpdate = (  from books in _context.Books
                                   where books.Id == book.Id
                                   select books).FirstOrDefault();
            if (BookToUpdate != null)
            {
                _context.Entry(BookToUpdate).CurrentValues.SetValues(book);
                _context.SaveChanges();
                return Ok(book);
            }
            else
            {
                return NotFound();
            }
            }
            catch{
                return BadRequest("Invalid data.");
            }
        }

        [HttpDelete]
        [Route("remove-book-by-id")]
        public ActionResult DeleteBook(int id)
        {
            try{
             Book BookToRemove = (from books in _context.Books
                               where books.Id == id
                               select books).FirstOrDefault();
            if ((BookToRemove) != null)
            {
               // _context.Entry(PersonToDelete).State = EntityState.Deleted;
               _context.Books.Remove(BookToRemove);
                _context.SaveChanges();
                return Ok();
            }            
            else
            {
                return NotFound();
            }
            }
             catch{
                return BadRequest("Invalid data.");
            }
        }

    }
}
 